<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
 header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: PUT, GET, POST, DELETE');
header('Access-Control-Allow-Headers: Authorization, Content-Type'); 

Route::group(['prefix' => 'guest'], function ()
{
	Route::group(['prefix' => 'user'], function ()
	{
		Route::post('store', 'Guest\UserController@store');
	});
	Route::group(['prefix' => 'auth'], function ()
	{
		Route::post('login', 'Guest\AuthController@login');
	});
});

Route::group(['prefix' => 'account'], function ()
{
	Route::get('show', 'Auth\AccountController@show');
});
Route::group(['prefix' => 'transaction'], function ()
{
	Route::get('users', 	'Auth\TransactionController@users');
	Route::post('store', 		'Auth\TransactionController@store');
	Route::get('index', 		'Auth\TransactionController@index');
});
Route::group(['prefix' => 'notification'], function ()
{
	Route::get('index',	'Auth\NotificationController@index');
	Route::put('read', 	'Auth\NotificationController@read');
});
Route::group(['prefix' => 'console'], function ()
{
	Route::put('accrue', 	'CVController@accrue');
	Route::put('reset', 	'CVController@reset');
});
