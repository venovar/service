<?php

return [
    'generic_error'     => 'Sorry, we could not complete your request. Contact us.',
    'account_created'	=> 'Account has been created.',
    'transaction_created'   => 'Transaction has been created.',
    'notification_readed'   => 'Notification(s) readed.'
];