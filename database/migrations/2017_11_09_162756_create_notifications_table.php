<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationsTable extends Migration
{
    const FK_NOTIF_USER = 'fk_notif_user';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('user_id')->index(self::FK_NOTIF_USER . '_idx');
            $table->string('title', 100);
            $table->string('details', 500)->nullable();
            $table->boolean('readed')->default(0);
			$table->integer('notifable_id')->nullable();
			$table->string('notifable_type', 50)->nullable();
            $table->timestamps();
			$table->softDeletes();
        });
        Schema::table('notifications', function(Blueprint $table)
        {
            $table
                ->foreign('user_id', self::FK_NOTIF_USER)
                ->references('id')
                ->on('users')
                ->onUpdate('NO ACTION')
                ->onDelete('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notifications', function(Blueprint $table)
        {
          $table->dropForeign(self::FK_NOTIF_USER);
        });
        Schema::dropIfExists('notifications');
    }
}
