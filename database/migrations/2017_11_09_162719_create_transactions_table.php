<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    const FK_TRANS_FROM     = 'fk_trans_from_user';
    const FK_TRANS_TO       = 'fk_trans_to_user';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('from_id')->index(self::FK_TRANS_FROM . '_idx');
            $table->integer('to_id')->index(self::FK_TRANS_TO . '_idx');
            $table->decimal('amount', 8, 2)->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
        
        Schema::table('transactions', function(Blueprint $table)
        {
            $table
                ->foreign('from_id', self::FK_TRANS_FROM)
                ->references('id')
                ->on('users')
                ->onUpdate('NO ACTION')
                ->onDelete('NO ACTION');
            $table
                ->foreign('to_id', self::FK_TRANS_TO)
                ->references('id')
                ->on('users')
                ->onUpdate('NO ACTION')
                ->onDelete('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('transactions', function(Blueprint $table)
		{
            $table->dropForeign(self::FK_TRANS_FROM);
            $table->dropForeign(self::FK_TRANS_TO);
        });
        Schema::dropIfExists('transactions');
    }
}
