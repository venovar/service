<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class CV extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'CV';
    }

    /**
     * Returns the next lowest integer value (as decimal) by rounding down value if necessary. 
     * With precision (number of digits (2) after the decimal point)
     * 
     * @param $value 
     * @return Floor value (0.00)
     */
    protected static function floor($value)
    {
        $exp = 2;
        return floor( $value * pow(10, $exp)) / pow(10, $exp);
    }
}
