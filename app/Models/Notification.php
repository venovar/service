<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    /**
     * FIELDS
     *
     * user_id
     * title
     * details
     * readed
     * notifable_id
     * notifable_type
     * created_at
     * updated_at
     * deleted_at
     */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'title',
        'details',
        'readed',
        'notifable_id',
        'notifable_type',
    ];

    /**
     * The attributes that should be hidden for arrays.
     */
    protected $hidden = [
        'deleted_at',
    ];

    /**
     * Polimorphism.
     */
    public function notifable()
    {
        return $this->morphTo();
    }
}
