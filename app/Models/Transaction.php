<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
    use SoftDeletes;

    /**
     * FIELDS
     *
     * from_id
     * to_id
     * amount
     * created_at
     * updated_at
     * deleted_at
    */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'from_id',
        'to_id',
        'amount'
    ];

    /**
     * The attributes that should be hidden for arrays.
     */
    protected $hidden = [
        'deleted_at',
    ];

    public function from ()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function to ()
    {
        return $this->belongsTo('App\Models\User');
    }
}
