<?php

namespace App\Http\Controllers\Guest;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\User;

class UserController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name'              => 'required|max:255',
            'email'             => 'required|email|max:255|unique:users,email,NULL,id',
            'password'          => sprintf('required|min:6|max:20|confirmed|password_vulnerability'),
        ];
        $validator = Validator::make($request->all(), $rules);
        if (!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        $user = new User();
        $user->fill($request->only(['name', 'email']));
        $user->password = bcrypt($request->password);
        if ($user->save()) {
            return response()->json([
                'message'   => __('messages.account_created')
            ]);
        }
        return response()->json([
            'errors' => ['message' => __('messages.generic_error')]
        ], 400);
    }
}
