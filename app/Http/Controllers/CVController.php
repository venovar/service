<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use DB;
class CVController extends Controller
{
    public function accrue()
    {
    	DB::table('users')->update(['amount' => DB::raw('amount + 0.25')]);
    }
    public function reset() 
    {
    	DB::table('users')->update(['amount' => 0]);
    }
}
