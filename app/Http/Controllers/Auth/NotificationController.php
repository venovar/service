<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Notification;
use Illuminate\Support\Facades\Validator;

class NotificationController extends Controller
{
    /**
     * Constructor with API middleware.
     */
    public function __construct()
    {
        $this->middleware(['auth:api']);
    }

    /**
     * Display a listing of the Notification.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $notifications = Notification::with('notifable')
            ->orderBy('readed', 'ASC')
            ->orderBy('id', 'DESC')
            ->whereUserId($request->user()->id)
            ->paginate(10)
            ->toArray();
        $notifications['total_unread'] = Notification::whereReaded(0)->whereUserId($request->user()->id)->count();
        return response()->json(compact('notifications'));
    }

    /**
     * Set as readed the notifications.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function read(Request $request)
    {
        $rules = [
            'notification'      => 'required|array|min:1|max:10',
            'notification.*.id' => sprintf('required|integer|distinct|exists:notifications,id,deleted_at,NULL,user_id,%d', $request->user()->id),
        ];
        $validator = Validator::make($request->all(), $rules);
        if (!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        if(Notification::whereIn('id', array_column($request->notification, 'id'))->update(['readed' => 1])) {
            return response()->json([
                'message'   => __('messages.notification_readed')
            ]);
        }
        return response()->json([
            'errors' => ['message' => __('messages.generic_error')]
        ], 400);
    }
}
