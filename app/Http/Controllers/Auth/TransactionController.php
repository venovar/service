<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\Transaction;
use App\Models\Notification;
use App\Models\User;
use App\Facades\CV;
use App\Events\NotificationEvent;

class TransactionController extends Controller
{
    /**
     * Constructor with API middleware.
     */
    public function __construct()
    {
        $this->middleware(['auth:api']);
    }

    /**
     * Display a listing of the Transaction.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $transactions = Transaction::with(['from', 'to'])
            ->orderBy('id', 'DESC')
            ->Where('from_id', '=', $request->user()->id)
            ->orwhere('to_id', '=', $request->user()->id)
            ->paginate(30);
        return response()->json(compact('transactions'));
    }

    /**
     * Provides user list.
     *
     * @return \Illuminate\Http\Response
     */
    public function users(Request $request)
    {
        $rules = [
            'term' => 'string|max:100',
        ];
        $validator = Validator::make($request->all(), $rules);
        if (!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        $users = User::where('id', '!=', $request->user()->id);
        if($request->has('term')) {
            if(strpos($request->term, '@'))
                $request->term = str_replace('@', ' ', $request->term);
            $users->selectRaw(sprintf('*, MATCH(name, email) AGAINST ("%s*" IN BOOLEAN MODE) AS score01', $request->term));
            $users->whereRaw(sprintf('MATCH(name, email) AGAINST ("%s*" IN BOOLEAN MODE)', $request->term));
            $users->orderBy('score01', 'DESC');
        }
        $users = $users
            ->orderBy('name', 'ASC')
            ->paginate(30);
        return response()->json(compact('users'));
    }

    /**
     * Debit from user amount.
     */
    private function debit(User $from, float $amount)
    {
        // Subtract only what was sent and not the one entered in the request.
        $from->amount -= $amount;
        if ($from->save()) {
            return $from;
        } else {
            \Log::error("Debit error!");
            \Log::error([$from->toArray(), $amount]);
            return null;
        }
    }
    
    /**
     * Store debit notification.
     *
     * @param User $from
     * @param User $to
     * @param float $amount
     * @param $transaction
     */
    private function debitNotification(User $from, User $to, float $amount, Transaction $transaction)
    {
        if ($notification = Notification::create([
            'user_id'           => $from->id,
            'title'             => __("Debit"),
            'details'           => sprintf("You sent CV %.2f to %s.", $amount, $to->name),
            'notifable_id'      => $transaction->id,
            'notifable_type'    => get_class($transaction)
        ])) {
            broadcast(new NotificationEvent($from, $notification));
        } else {
            \Log::error("Debit Notification error!");
            \Log::error([$from->toArray(), $amount]);
        }
    }
        
    /**
     * Store credit notification.
     *
     * @param User $from
     * @param User $to
     * @param float $amount
     * @param $transaction
     */
    private function creditNotification(User $from, User $to, float $amount, Transaction $transaction)
    {
        if ($notification = Notification::create([
            'user_id'           => $to->id,
            'title'             => __("Credit"),
            'details'           => sprintf("You received CV %.2f from %s.", $amount, $from->name),
            'notifable_id'      => $transaction->id,
            'notifable_type'    => get_class($transaction)
        ])) {
            broadcast(new NotificationEvent($to, $notification));
        } else {
            \Log::error("Credit Notification error!");
            \Log::error([$from->toArray(), $amount]);
        }
    }
    
    /**
     * Record the history transaction.
     *
     * @param User $from
     * @param User $to
     * @param float $amount
     */
    private function transactionHistory(User $from, User $to, float $amount)
    {
        if ($transaction = Transaction::create([
            'from_id'   => $from->id,
            'to_id'     => $to->id,
            'amount'    => $amount,
        ])) {
            $this->debitNotification($from, $to, $amount, $transaction);
            $this->creditNotification($from, $to, $amount, $transaction);
        } else {
            \Log::error("Transaction History error!");
            \Log::error([$from->toArray(), $amount]);
        }
    }

    /**
     * Sends the values to the favored.
     *
     * @param User $from
     * @param array $to
     * @param float $amount
     * @return float The sum after the value divided with the 'floor'.
     */
    private function send(User $from, array $to, float $amount)
    {
        // Rounds the result to floor so there is no division down after two decimal places.
        $amountForEach = CV::floor($amount / count($to));
        foreach ($to as $favored) {
            $user = User::find($favored['id']);
            if ($user) {
                $user->amount += $amountForEach;
                $from = $this->debit($from, $amountForEach);
                if ($from && $user->save()) {
                    $this->transactionHistory($from, $user, $amountForEach);
                    continue;
                }
            }
            \Log::error("Transfer error!");
            \Log::error([$from->toArray(), $to, $amount]);
        }
        return $amountForEach * count($to);
    }

    /**
     * Store a newly created Transaction in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'to'        => 'required|array|min:1|max:10',
            'to.*.id'   => sprintf('required|integer|distinct|not_in:%d|exists:users,id,deleted_at,NULL', $request->user()->id),
            'amount'    => sprintf('required|numeric|min:0.01|max:%.2f|check_apportionment', $request->user()->amount),
        ];
        $validator = Validator::make($request->all(), $rules);
        if (!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        // Totaly sent.
        $newAmount = $this->send($request->user(), $request->to, $request->amount);
        return response()->json([
            'message'   => __('messages.transaction_created')
        ]);
    }
}
