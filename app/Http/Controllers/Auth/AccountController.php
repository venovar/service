<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Models\User;

class AccountController extends Controller
{
    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->middleware(['auth:api']);
    }
    
    /**
     * Display the specified User.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        if ($request->has('id')) {
            $rules = [
                'id' => 'required|integer|exists:users,id,deleted_at,NULL'
            ];
            $validator = Validator::make($request->all(), $rules);
            if (!$validator->passes()) {
                return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
                ], 400);
            }
            $user = User::find($request->id);
        } else {
            $user = $request->user();
        }
        $user->makeVisible('amount');
        return response()->json(compact('user'));
    }
}
