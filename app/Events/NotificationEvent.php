<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Models\User;
use App\Models\Notification;

class NotificationEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    
    public $user;
    
    public $notification;
    
    private function onlyAttributs ($data, $except) {
        $data = $data->makeVisible(['amount'])->toArray();
        foreach ($data as $key => $value) {
            if(!in_array($key, $except)) {
                unset($data[$key]);
            }
        }
        return $data;
    }
    
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user, Notification $notification)
    {
        $this->user = $this->onlyAttributs($user, ['id', 'amount']);
        $this->notification = $this->onlyAttributs($notification, ['id', 'title', 'details', 'readed', 'created_at']);
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('transaction-' . $this->user['id']);
    }
}
