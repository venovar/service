<?php

namespace App\Validators;
 
use Illuminate\Validation\Validator;
use App\Facades\CV;
use Auth;

class CustomValidator extends Validator
{

    /**
     * Verify if the password is strong.
     * 
     * @param $attribute
     * @param $value
     * @param $parameters
     * @return Boolean
     */
    public function validatePasswordVulnerability($attribute, $value, $parameters)
    {
        if (!count($this->messages->toArray())) {
            if (!preg_match_all('/[A-Z]/', $value, $match) ||
                !preg_match_all('/[a-z]/', $value, $match) ||
                !preg_match_all('/[0-9]/', $value, $match) ||
                !preg_match("/\b[+&@#\/%?=~_|!:,.;]/i", $value, $match)
            ) {
                return false;
            }
        }
        return true;
    }

    /**
     * Check apportionment between favored.
     * 
     * @param $attribute
     * @param $value
     * @param $parameters
     * @return Boolean
     */
    public function validateCheckApportionment($attribute, $value, $parameters)
    {
        if (!count($this->messages->toArray())) {
            return CV::floor($value / count($this->data['to'])) > 0.00;
        }
        return true;
    }
}